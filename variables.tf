variable "instance_type" {
    type = string
    default = "t3.medium"
  
}

variable "ami" {
    type = string
    default = "1234567890"
}